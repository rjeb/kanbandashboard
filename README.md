#  Kanban Board : React + TypeScript + Vite
This project is a Kanban Board application built with React. It allows you to create and manage columns and tasks using a drag-and-drop interface.

## Features
Create new columns
Add tasks to columns
Drag and drop tasks between columns
Delete columns and tasks


## Usage
To create a new column, click on the "Add column" button.
To add a task to a column, click on the "Add task" button in the respective column.
To move a task between columns, simply drag and drop it to the desired column.
To delete a column or a task, click on the delete icon next to it.
Contributing
Contributions are welcome! If you have any ideas, improvements, or bug fixes, please open an issue or submit a pull request.

License
This project is licensed under the MIT License. See the LICENSE file for details.

