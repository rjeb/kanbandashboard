import PlusIcon from '../icons/PlusIcon';
import { Key, useMemo, useState } from 'react';
import { Column, Id, Task } from '../types';
import ColumnContainer from './ColumnContainer';
import { DndContext, DragEndEvent, DragOverEvent, DragOverlay, DragStartEvent, MouseSensor, PointerSensor, UniqueIdentifier, useSensor } from '@dnd-kit/core';
import { SortableContext, arrayMove } from '@dnd-kit/sortable';
import { createPortal } from 'react-dom';
import { useSensors} from '@dnd-kit/core';
import TaskCard from './TaskCard';


function KanbanBoard() {
    const [columns, setColumns] = useState<Column[]>([]);
    const ColumnsId: UniqueIdentifier[] = useMemo(() => columns.map((col) => col.id as UniqueIdentifier),
         [columns]);

    const [tasks, setTasks] = useState<Task[]>([]);

    const [activeColumn, setActiveColumn] = useState<Column | null>(null);

    const [activeTask, setActiveTask] = useState<Task | null>(null);
    
    const sensors = useSensors(
        useSensor(PointerSensor, {
          activationConstraint: {
            distance: 3,
          },
        })
      );

    return (
        <div className="
            m-auto
            flex
            min-h-screen
            w-full
            items-center   
            overflow-x-auto
            overflow-y-hidden  
            px-[40px]
        ">
            <DndContext 
                sensors={sensors} 
                onDragStart={onDragStart} 
                onDragEnd={onDragEnd}
                onDragOver={onDragOver}
            >
                <div className="m-auto flex gap-2"> 
                    <div className="flex gap-4">
                        <SortableContext items={ColumnsId}>
                            {columns.map((col) => (
                                <ColumnContainer 
                                    key={col.id as Key} 
                                    column={col} 
                                    deleteColumn={deleteColumn} 
                                    updateColumn={updateColumn}
                                    createTask={createTask}
                                    updateTask={updateTask}
                                    deleteTask={deleteTask}
                                    tasks={tasks.filter((task) => task.columnId === col.id)}
                                />
                            ))}
                        </SortableContext>
                    </div>
                    <button 
                        onClick={
                            () => {
                                CreateNewColumn();
                            }
                        }
                        className="
                    h-[60px]
                    w-[350px]
                    min-w-[350px]
                    cursor-pointer
                    rounded-lg
                    bg-mainBackgroundColor
                    border-2
                    border-columnBackgroundColor
                    p-4
                    ring-rose-500
                    hover:ring-2
                    flex
                    gap-2
                    "
                    >
                        <PlusIcon /> 
                        Add column
                    </button>
                </div>
                {createPortal(
                    <DragOverlay>
                        {activeColumn && (
                            <ColumnContainer 
                                column={activeColumn} 
                                deleteColumn={deleteColumn} 
                                updateColumn={updateColumn}
                                createTask={createTask}
                                updateTask={updateTask}
                                deleteTask={deleteTask}
                                tasks={tasks.filter(
                                    (task) => task.columnId === activeColumn.id
                                )}
                                { ...activeTask && (
                                    <TaskCard
                                      task={activeTask}
                                      deleteTask={deleteTask}
                                      updateTask={updateTask}
                                    />
                                )}
                            />
                        )}
                    </DragOverlay>, document.body
                )}
            </DndContext>
        </div>
    );

    function createTask(columnId: Id) {
        const newTask: Task = {
            id: generateId(),
            columnId,
            content: `Task ${tasks.length + 1}`,
        };

        setTasks([...tasks, newTask]);
    }

    function updateTask(id: Id, content: string) {
        const newTasks = tasks.map((task) => {
            if (task.id === id) {
                return {
                    ...task,
                    content,
                };
            }
            return task;
        });
        setTasks(newTasks);
    }

    function deleteTask(id: Id) {
        const newTasks = tasks.filter((task) => task.id !== id);
        setTasks(newTasks);
    }

    function CreateNewColumn() {
        const columnToAdd: Column = {
            id: generateId(),
            title: `Column ${columns.length + 1}`,
        };

        setColumns([...columns, columnToAdd]);
    }

    function deleteColumn(id: Id) {
        const filteredColumns = columns.filter((col) => col.id !== id);
        setColumns(filteredColumns);

        const newTasks = tasks.filter((task) => task.columnId !== id);
        setTasks(newTasks);
    }

    function updateColumn(id: Id, title: string) {
        const newColumns = columns.map((col) => {
            if (col.id === id) {
                return {
                    ...col,
                    title,
                };
            }
            return col;
        });
        setColumns(newColumns);
    }

    function onDragStart(event: DragStartEvent) {
        if (event.active.data.current?.type === "Column") {
            setActiveColumn(event.active.data.current as Column);
            return;
        }

        if (event.active.data.current?.type === "Task") {
            setActiveTask(event.active.data.current as Task);
            return;
        }
    }

    function onDragEnd(event: DragEndEvent) {
        setActiveColumn(null);
        setActiveTask(null);
        const { active, over } = event;
        if (!over) {
            return;
        }

        const activeColumnId = active?.id as UniqueIdentifier;
        const overColumnId = over?.id as UniqueIdentifier;

        if (activeColumnId === overColumnId) {
            return;
        }

        setColumns( columns => {
            const activeIndex = columns.findIndex(col => col.id === activeColumnId);
            const overIndex = columns.findIndex(col => col.id === overColumnId);
            return arrayMove(columns, activeIndex, overIndex);
        });
    }

    function onDragOver(event: DragOverEvent) {
        const { active, over } = event;
        if (!active || !over) {
            return;
        }
    
        const activeColumnId = active?.id as UniqueIdentifier;
        const activeType = active?.data.current?.type;
        const activeId = active?.data.current?.id;
    
        const overColumnId = over?.id as UniqueIdentifier;
        const overType = over?.data.current?.type;
        const overId = over?.data.current?.id;
    
        if (activeColumnId === overColumnId) {
            return;
        }
    
        const isActiveATask = activeType === "Task";
        const isOverATask = overType === "Task";

        if(!isActiveATask) {
            return;
        }
    
        if (isActiveATask && isOverATask) {
            setTasks((tasks) => {
                const activeIndex = tasks.findIndex((task) => task.id === activeId);
                const overIndex = tasks.findIndex((task) => task.id === overId);
    
                if (activeIndex !== -1 && overIndex !== -1) {
                    tasks[activeIndex].columnId = tasks[overIndex].columnId;
                }
    
                return arrayMove(tasks, activeIndex, overIndex);
            });
        }
        const isOverAColumn = overType === "Column";
        if (!isActiveATask && isOverAColumn) {
            setTasks((tasks) => {
                const activeIndex = tasks.findIndex((task) => task.id === activeId);

                tasks[activeIndex].columnId = overId;  
    
                return arrayMove(tasks, activeIndex, activeIndex);
            });
        }
    }
    
}

function generateId() {
    /** Generate a random number between 0 and 10000 */
    return Math.floor(Math.random() * 10001);
}

export default KanbanBoard;